def isVowel(str):
	if len(str) > 1:
		return "String must be 1 character long only"
		
	str = str.lower()
		
	if str in ["a", "e", "i", "o", "u"]:
		return True
	else:
		return False
	
	
print "aa : ", isVowel("aa")
print "t : ", isVowel("t")
print "a : ", isVowel("a")