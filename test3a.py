from sys import argv

max = 1
max = int(argv[1])
str = ""
times = ""
for n in range(1,max+1):
	str += ("\t%d")%n

print("\t"+str+"\n\t"+("\t-----"*max+"\t"))

for i in range(1,max+1):
	times = ("%d\t| ")%i
	for j in range(1,max+1):
		times += ("\t%d")%(i*j)
	print(times)
	