import sys
import re

file = open(sys.argv[1])
txt = file.read()
txt = txt.split()

lenHist ={}

for t in txt:
	t = re.sub("[^A-Za-z]", "", t) 
	lg = len(t)
	if lg in lenHist:
		lenHist[lg] += 1
	else:
		lenHist[lg] = 1
		
for elem in lenHist:
	print ("%2s : %s")%(elem,lenHist[elem]*"*")

