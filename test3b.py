import random

answer = random.randint(1,100)
guess = 0
numberOfTries = 0

while(guess != answer):
	guess = raw_input("\nguess a number\n> ")
	try:
		guess = int(guess)
	except:
		print("invalid input. try again")
		continue
	if(guess > answer):
		print("> Too high")
	elif(guess < answer):
		print("> Too low")
		
	numberOfTries+=1

if(guess == answer):
	print("> CORRECT! Total number of tries: %d")%numberOfTries